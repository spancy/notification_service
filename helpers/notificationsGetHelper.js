var { QueryBuilder } = require("objection");
var Notification = require("../models/Usernotification");

//function to get all templates and its underlying relations
async function notificationGetData(req, queryBy) {
  try {
    const nsData = await Notification.query().where(queryBy, req.params.id);

    let message = "";

    message = {
      statusCode: 200,
      body: nsData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function notificationGetTaskReceiverData(req, queryByTask, queryByReceiver) {
  try {
    const notificationGetTaskReceiverData = await Notification.query()
      .where(queryByTask, req.params.taskid)
      .where(queryByReceiver, req.params.receiverid);

    let message = "";

    message = {
      statusCode: 200,
      body: notificationGetTaskReceiverData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

async function notificationReceiverData(req, queryByReceiver) {
  try {
    const notificationGetReceiverData = await Notification.query()
      .where(queryByReceiver, req.params.receiver_id);

    let message = "";

    message = {
      statusCode: 200,
      body: notificationGetReceiverData
    };

    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "Errored : " + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

//function to get all templates and its underlying relations

module.exports.notificationGetData = notificationGetData;
module.exports.notificationGetTaskReceiverData = notificationGetTaskReceiverData;
module.exports.notificationReceiverData = notificationReceiverData;
