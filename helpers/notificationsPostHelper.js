var { QueryBuilder } = require("objection");
var Notification = require("../models/Usernotification");
var notificationType= require("../models/Notificationtype");
//function to insert into template table and relations.
async function notificationsInsert(req) {
  const notificationInsert = {
    sender_id: req.body.sender_id,
    receiver_id: req.body.receiver_id,
    appointment_id: req.body.sender_id,
    title: req.body.title,
    type:req.body.type,
    message: req.body.message
  };
  //type:notificationType.attributes.code,
  //type:type,
  try {
    let message = "";
    const feedback = await Notification.query().insertGraph(notificationInsert);

    message = {
      statusCode: 200,
      body: {
        message: "New Notification Created",
        id: feedback.id
      }
    };

    console.log(message);
    //  res.json(message);
    return message;
  } catch (e) {
    message = {
      statusCode: 500,
      body: "New Notification Errored" + e
    };
    console.log(message);
    //res.json(message);
    return message;
  }
}

module.exports.notificationsInsert = notificationsInsert;
