exports.up = function(knex, Promise) {
  return Promise.all([
    
    knex.schema.createTable('notification_group', function(table){
        table.increments('id').primary();
        table.integer('code').unique().notNullable();
        table.string('name').unique().notNullable();
        table.timestamps();
    }).then(function () {
            return knex('notification_group').insert([
                {code: '1', name: 'CUSTOMER'},
                {code: '2', name: 'SALON'},
                {code: '3', name: 'PAYMENT'},
                {code: '4', name: 'CREDITS'},
                {code: '5', name: 'APPOINTMENT'}
            ]);
        }
    ),  
    
    knex.schema.createTable('notification_type', function(table){
          table.increments('id').primary();
          table.integer('code').unique();
          table.string('name').unique();
          table.integer('group').unsigned().references('notification_group.code');
          table.timestamps();
      }).then(function () {
              return knex('notification_type').insert([
                  {code: 1, name: 'CUSTOMER_KYC_COMPLETE', group: 1},
                  {code: 2, name: 'SALON_KYC_COMPLETE', group: 2},
                  {code: 3, name: 'SUCCESSFUL_BOOKING', group: 5},
                  {code: 4, name: 'FAILED_BOOKING', group: 1},
                  {code: 5, name: 'PAYMENT_COMPLETE', group: 3},
                  {code: 6, name: 'PAYMENT_FAILED', group: 3},
                  {code: 7, name: 'REQ_FOR_PAYMENT', group: 3},
                  {code: 8, name: 'APPOINTMENT_REMINDER', group: 5},
                  {code: 9, name: 'FEEDBACK_RECEIVED', group: 5},
                  {code: 10, name: 'APPOINTMENT_COMPLETE', group: 5},
                  {code: 11, name: 'APPOINTMENT_CANCELLED_CUSTOMER', group: 5},
                  {code: 12, name: 'APPOINTMENT_CANCELLED_SALON', group: 5},
                  {code: 13, name: 'BOOKING_INITIATED', group: 1}
              ]);
          }
      ),

      knex.schema.createTable('user_notification', function(table){
          table.increments('id').primary();
          table.integer('sender_id').unsigned();
          table.integer('receiver_id').unsigned();
          table.integer('appointment_id').unsigned();
          table.biginteger('created_time_at_source');
          table.integer('type').unsigned().references('notification_type.code');
          table.string('title').notNullable();
          table.string('message').notNullable();
          table.boolean('mark_as_read').notNullable().defaultTo(false);
          table.boolean('dismissed').notNullable().defaultTo(false);
          table.boolean('deleted').notNullable().defaultTo(false);
          table.timestamps('true','true');
      })
      .createTable('notification_preference', function(table){
          table.increments('id').primary();
          table.integer('user_id').unique().notNullable();
          table.integer('type').unsigned().references('notification_type.code');
          table.boolean('enabled').notNullable().defaultTo(false);
          table.boolean('email_ok').notNullable().defaultTo(false);
          table.boolean('sms_ok').notNullable().defaultTo(false);
          table.boolean('web_ok').notNullable().defaultTo(false);
          table.timestamps();
          table.unique(['user_id', 'type']);
      })
      .createTable('notification_template', function(table){
          table.increments('id').primary();
          table.integer('type').unsigned().references('notification_type.code');
          table.string('email_template_ref');
          table.string('sms_template_ref');
          table.timestamps();
      })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('user_notification'),
    knex.schema.dropTableIfExists('notification_type'),
    knex.schema.dropTableIfExists('notification_group'),
    knex.schema.dropTableIfExists('notification_preference'),
    knex.schema.dropTableIfExists('notification_template')
  ])
};
