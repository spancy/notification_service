"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class Notificationgroup extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "notification_group";
  }
}

module.exports = Notificationgroup;