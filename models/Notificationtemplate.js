var Bookshelf = require('../bookshelf');
var { NotificationType } = require('../db/notificationtype');

var NotificationTemplate = Bookshelf.Model.extend({
    tableName: 'notification_template',
    type: function() {
        return this.belongsTo(NotificationType);
    },
    hasTimestamps: true
});

var NotificationTemplates = Bookshelf.Collection.extend({
  model: NotificationTemplate
});

module.exports.NotificationTemplate = NotificationTemplate;
module.exports.NotificationTemplates = NotificationTemplates;
