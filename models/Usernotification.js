"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class Usernotification extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "user_notification";
  }

  static get relationMappings() {
    return {
      notificationtype: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/Notificationtype",
        join: {
          from: "notification_type.code",
          to: "user_notification.type"
        }
      }
    };
  }
}
module.exports = Usernotification;
