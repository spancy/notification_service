var Bookshelf = require('../bookshelf');
var { NotificationType } = require('../db/notificationtype');

var NotificationPreference = Bookshelf.Model.extend({
    tableName: 'notification_preference',
    type: function() {
        return this.belongsTo(NotificationType);
    },
    hasTimestamps: true
});

var NotificationPreferences = Bookshelf.Collection.extend({
  model: NotificationPreference
});

module.exports.NotificationPreference = NotificationPreference;
module.exports.NotificationPreferences = NotificationPreferences;
