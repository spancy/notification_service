"use strict";

const Model = require("objection").Model;
const knex = require("knex");

const KnexConfig = require("../knexfile");

Model.knex(knex(KnexConfig.development));
class Notificationtype extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "notification_type";
  }

  static get relationMappings() {
    return {
        notificationgroup: {
        relation: Model.HasManyRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + "/NotificationGroup",
        join: {
          from: "notification_group.code",
          to: "notification_type.group"
        }
      }
    };
  }
}

module.exports = Notificationtype;
