var express = require("express");
var router = express.Router();

var postHelper = require("../helpers/notificationsPostHelper");
var getHelper = require("../helpers/notificationsGetHelper");

const knex = require("knex");

router
  .route("/")

  //gets called when a new template is added/ a task is created for the first time since it requires the relation to be mapped
  .post(async function(req, res) {
    let response = await postHelper.notificationsInsert(req);
    res.json(response);
  });

router
  .route("/getByReceiverID/:receiver_id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.notificationReceiverData(req, "receiver_id");
    res.json(response);
  });

router   
  .route("/getByID/:id")
  // fetch all addresses
  .get(async function(req, res) {
    let response = await getHelper.notificationGetData(req, "id");
    res.json(response);
  });
  
module.exports = router;
